package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.lazareva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.lazareva.tm.dto.response.ApplicationAboutResponse;
import ru.t1.lazareva.tm.dto.response.ApplicationVersionResponse;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.service.PropertyService;

@Category(SoapCategory.class)
public final class SystemEndpointTest {

    @NotNull
    private final static IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private final static ISystemEndpoint ENDPOINT = ISystemEndpoint.newInstance(PROPERTY_SERVICE);

    @BeforeClass
    public static void before() {
    }

    @AfterClass
    public static void after() {
    }

    @Test
    public void getAbout() {
        @NotNull final ApplicationAboutResponse response = ENDPOINT.getAbout(new ApplicationAboutRequest());
        Assert.assertNotNull(response);
    }

    @Test
    public void getVersion() {
        @NotNull final ApplicationVersionResponse response = ENDPOINT.getVersion(new ApplicationVersionRequest());
        Assert.assertNotNull(response);
    }

}
