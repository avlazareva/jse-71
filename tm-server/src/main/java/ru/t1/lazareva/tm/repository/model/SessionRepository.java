package ru.t1.lazareva.tm.repository.model;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.Session;

import java.util.Comparator;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    @NotNull
    Class<Session> getClazz();

    @NotNull String getSortColumnName(@NotNull Comparator comparator);
}
