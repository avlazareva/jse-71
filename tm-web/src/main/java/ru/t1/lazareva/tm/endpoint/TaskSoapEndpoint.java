package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.soap.*;
import ru.t1.lazareva.tm.util.UserUtil;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.lazareva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskFindOneByIdResponse findOne(@RequestPayload final TaskFindOneByIdRequest request) {
        return new TaskFindOneByIdResponse(taskService.findOneById(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskSaveResponse saveOne(@RequestPayload final TaskSaveRequest request) {
        taskService.save(UserUtil.getUserId(), request.getTask());
        return new TaskSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskUpdateResponse updateOne(@RequestPayload final TaskUpdateRequest request) {
        taskService.save(UserUtil.getUserId(), request.getTask());
        return new TaskUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskDeleteResponse deleteOneById(@RequestPayload final TaskDeleteRequest request) {
        taskService.removeOneById(UserUtil.getUserId(), request.getId());
        return new TaskDeleteResponse();
    }

}
