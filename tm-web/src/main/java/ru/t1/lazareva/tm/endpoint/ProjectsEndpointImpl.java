package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.endpoint.IProjectsEndpoint;
import ru.t1.lazareva.tm.api.service.IProjectDtoService;
import ru.t1.lazareva.tm.dto.ProjectDTO;
import ru.t1.lazareva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.lazareva.tm.api.endpoint.IProjectsEndpoint", serviceName = "ProjectsEndpointImplService", name = "ProjectsEndpointImplService")
public class ProjectsEndpointImpl implements IProjectsEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public List<ProjectDTO> findAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void saveAll(@WebParam(name = "projects", partName = "projects") @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void updateAll(@WebParam(name = "projects", partName = "projects") @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @DeleteMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void deleteAll(@WebParam(name = "projects", partName = "projects") @NotNull @RequestBody List<ProjectDTO> projects) {
        projectService.removeAll(UserUtil.getUserId());
    }

}