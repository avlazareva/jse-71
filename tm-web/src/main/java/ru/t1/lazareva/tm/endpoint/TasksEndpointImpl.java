package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.endpoint.ITasksEndpoint;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.lazareva.tm.api.endpoint.ITasksEndpoint", serviceName = "TasksEndpointImplService", name = "TasksEndpointImplService")
public class TasksEndpointImpl implements ITasksEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public List<TaskDTO> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void saveAll(@WebParam(name = "tasks", partName = "tasks") @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void updateAll(@WebParam(name = "tasks", partName = "tasks") @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping()
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void deleteAll() {
        taskService.removeAll(UserUtil.getUserId());
    }

}