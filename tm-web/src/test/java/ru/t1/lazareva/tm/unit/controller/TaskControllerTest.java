package ru.t1.lazareva.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.lazareva.tm.api.service.ITaskDtoService;
import ru.t1.lazareva.tm.config.ApplicationConfiguration;
import ru.t1.lazareva.tm.config.WebApplicationConfiguration;
import ru.t1.lazareva.tm.dto.TaskDTO;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskControllerTest {

    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/tasks";
    @NotNull
    private static final String TASK_URL = "http://localhost:8080/task";
    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");
    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");
    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");
    @NotNull
    private final TaskDTO task4 = new TaskDTO("Test Task 4");
    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;
    @NotNull
    private MockMvc mockMvc;
    @NotNull
    @Autowired
    private WebApplicationContext wac;
    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskService.save(UserUtil.getUserId(), task1);
        taskService.save(UserUtil.getUserId(), task2);
    }

    @After
    public void clear() {
        taskService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(TASKS_URL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(TASK_URL + "/create"))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(3, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(TASK_URL + "/delete/" + task1.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(1, taskService.findAll(UserUtil.getUserId()).size());
        Assert.assertNull(
                taskService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> task1.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void editTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(TASK_URL + "/edit/" + task1.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
